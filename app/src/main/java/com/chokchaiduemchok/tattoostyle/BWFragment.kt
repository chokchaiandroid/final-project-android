package com.chokchaiduemchok.tattoostyle

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chokchaiduemchok.tattoostyle.databinding.FragmentBWBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BWFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BWFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentBWBinding? = null
    private val binding get() = _binding

    private lateinit var adapter: MyAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var newArrayList: ArrayList<Dataimg>

    lateinit var imageId : Array<Int>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBWBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataInitialize()
        val layoutManager = LinearLayoutManager(context)
        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(true)
        adapter = MyAdapter(newArrayList)
        recyclerView.adapter = adapter

        binding?.back1?.setOnClickListener {
            val action = BWFragmentDirections.actionBWFragmentToSketchFragment()
            view.findNavController().navigate(action)
        }
        binding?.home1?.setOnClickListener {
            val action = BWFragmentDirections.actionBWFragmentToHomeFragment()
            view.findNavController().navigate(action)
        }

    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BWFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            BWFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun dataInitialize(){
        newArrayList = arrayListOf<Dataimg>()

        imageId = arrayOf(
            R.drawable.bw1,
            R.drawable.bw2,
            R.drawable.bw3,
            R.drawable.bw4,
            R.drawable.bw5,
            R.drawable.bw6,
            R.drawable.bw7,
            R.drawable.bw8,
            R.drawable.bw9
        )
        for(i in imageId.indices){
            val dataimg = Dataimg(imageId[i])
            newArrayList.add(dataimg)
        }

    }

}