package com.chokchaiduemchok.tattoostyle

import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.imageview.ShapeableImageView

class MyAdapter (private val imgList: ArrayList<Dataimg>): RecyclerView.Adapter<MyAdapter.MyViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val imageView = LayoutInflater.from(parent.context).inflate(R.layout.list_item,parent,false)
        return MyViewHolder(imageView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = imgList[position]
        holder.tImage.setImageResource(currentItem.tImage)
    }

    override fun getItemCount(): Int {
        return imgList.size
    }


    class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){

        val tImage : ShapeableImageView = itemView.findViewById(R.id.tattoo_image)

    }

}